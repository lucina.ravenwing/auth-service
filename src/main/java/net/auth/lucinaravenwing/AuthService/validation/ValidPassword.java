package net.auth.lucinaravenwing.AuthService.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates that the given variable will need to pass a Password validation.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @since 3 May 2022
 */
@Constraint(validatedBy = PasswordConstraintValidator.class)
@Target({ElementType.TYPE, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidPassword {

    /**
     * Sets the method to be sent to the user. Defaults to "Invalid Password".
     *
     * @return the message.
     */
    String message() default "Invalid Password";

    /**
     * The class to apply the annotation to.
     *
     * @return the class that will be validated.
     */
    Class<?>[] groups() default {};

    /**
     * The class to apply the annotation to.
     *
     * @return the class that will be validated.
     */
    Class<? extends Payload>[] payload() default {};
}
