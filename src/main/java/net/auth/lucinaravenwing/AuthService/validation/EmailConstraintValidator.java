package net.auth.lucinaravenwing.AuthService.validation;


import net.auth.lucinaravenwing.AuthService.repos.UserRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Validator to ensure that an email is valid.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @since 3 May 2022
 */
public class EmailConstraintValidator implements ConstraintValidator<ValidEmail, String> {

    // TRACE, DEBUG, INFO, WARN, ERROR, FATAL
    private static final Logger logger = LoggerFactory.getLogger(EmailConstraintValidator.class);

    @Autowired
    private UserRepo userRepo;

    /**
     * Initializes the validator.
     *
     * @param email ValidEmail annotation.
     */
    @Override
    public void initialize(ValidEmail email) {
    }

    /**
     * Check if the email is valid.
     *
     * @param email   to validate.
     * @param context provides context for the validation.
     * @return whether the email is valid or not.
     */
    @Override
    public boolean isValid(String email, ConstraintValidatorContext context) {
        logger.debug("Email being validated.");

        if (!userRepo.existsByEmail(email)) {
            return true;
        }

        context.buildConstraintViolationWithTemplate("Email is already in use!")
                .addConstraintViolation()
                .disableDefaultConstraintViolation();

        return false;
    }

}
