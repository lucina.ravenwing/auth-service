package net.auth.lucinaravenwing.AuthService.validation;


import net.auth.lucinaravenwing.AuthService.repos.UserRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Validator to ensure that a username is valid.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @since 3 May 2022
 */
public class UsernameConstraintValidator implements ConstraintValidator<ValidUsername, String> {
    // TRACE, DEBUG, INFO, WARN, ERROR, FATAL
    private static final Logger logger = LoggerFactory.getLogger(UsernameConstraintValidator.class);

    @Autowired
    private UserRepo userRepo;

    /**
     * Initializes the validator.
     *
     * @param username ValidUsername annotation.
     */
    @Override
    public void initialize(ValidUsername username) {
    }

    /**
     * Check if the username is valid.
     *
     * @param username to validate.
     * @param context  provides context for the validation.
     * @return whether the username is valid or not.
     */
    @Override
    public boolean isValid(String username, ConstraintValidatorContext context) {
        logger.debug("Username being validated.");

        Pattern regexPattern = Pattern.compile("^[\\w-_.]*$");
        Matcher match = regexPattern.matcher(username);

        boolean isValid = match.find();
        boolean existsByUsername = userRepo.existsByUsername(username);

        if (isValid && username.length() >= 6 && username.length() <= 36 && !existsByUsername) {
            return true;
        }

        List<String> messageBuilder = new ArrayList<>();

        if (!isValid) {
            messageBuilder.add("Username contains illegal characters. ");
        }
        if (username.length() < 6 || username.length() > 36) {
            messageBuilder.add("Username length must be between 6 and 36 characters.");
        }
        if (existsByUsername) {
            messageBuilder.add("Username already taken. Please choose a different username.");
        }

        context.buildConstraintViolationWithTemplate(String.join(",", messageBuilder))
                .addConstraintViolation()
                .disableDefaultConstraintViolation();

        return false;

    }

}
