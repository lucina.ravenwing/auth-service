package net.auth.lucinaravenwing.AuthService.validation;

import lombok.SneakyThrows;
import org.passay.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

/**
 * Validator to ensure that a password is valid.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @since 3 May 2022
 */
public class PasswordConstraintValidator implements ConstraintValidator<ValidPassword, String> {
    // TRACE, DEBUG, INFO, WARN, ERROR, FATAL
    private static final Logger logger = LoggerFactory.getLogger(PasswordConstraintValidator.class);

    /**
     * Initializes the validator.
     *
     * @param password ValidPassword annotation.
     */
    @Override
    public void initialize(ValidPassword password) {
    }

    /**
     * Check if the email is valid.
     *
     * @param password to validate.
     * @param context  provides context for the validation.
     * @return whether the password is valid or not.
     */
    @SneakyThrows
    @Override
    public boolean isValid(String password, ConstraintValidatorContext context) {
        logger.debug("Password being validated.");

        PasswordValidator validator = new PasswordValidator(Arrays.asList(
                new LengthRule(8, 120),
                new CharacterRule(EnglishCharacterData.UpperCase, 1),
                new CharacterRule(EnglishCharacterData.LowerCase, 1),
                new CharacterRule(EnglishCharacterData.Digit, 1),
                new WhitespaceRule()
        ));

        RuleResult result = validator.validate(new PasswordData(password));

        if (result.isValid()) {
            logger.debug("Password validated.");

            return true;
        }

        List<String> messages = validator.getMessages(result);
        String messageTemplate = String.join(", ", messages);
        context.buildConstraintViolationWithTemplate(messageTemplate)
                .addConstraintViolation()
                .disableDefaultConstraintViolation();

        return false;
    }
}
