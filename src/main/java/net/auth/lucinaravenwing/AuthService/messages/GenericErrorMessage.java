package net.auth.lucinaravenwing.AuthService.messages;

import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * Generic ErrorMessage class that will be used to return to the user.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @since 3 May 2022
 */
@Getter
@Setter
public class GenericErrorMessage {

    // TRACE, DEBUG, INFO, WARN, ERROR, FATAL
    private static final Logger logger = LoggerFactory.getLogger(GenericErrorMessage.class);

    private final int statusCode;
    private final Date timestamp;
    private final String message;
    private final String description;

    /**
     * All args constructor for ErrorMessage objects.
     *
     * @param statusCode  of the error.
     * @param timestamp   of the error.
     * @param message     of the error.
     * @param description of the error.
     */
    public GenericErrorMessage(int statusCode, Date timestamp, String message, String description) {
        logger.warn("Error message throw with status {} and message {}.", statusCode, message);

        this.statusCode = statusCode;
        this.timestamp = timestamp;
        this.message = message;
        this.description = description;
    }

}
