package net.auth.lucinaravenwing.AuthService.messages;

import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import java.util.Map;

/**
 * Custom error message for registration errors.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @since 3 May 2022
 */
@Getter
@Setter
public class RegisterErrorMessage {

    // TRACE, DEBUG, INFO, WARN, ERROR, FATAL
    private static final Logger logger = LoggerFactory.getLogger(RegisterErrorMessage.class);

    private final int statusCode;
    private final Map<String, String> message;
    private final String description;

    /**
     * All args constructor for RegisterErrorMessage objects.
     *
     * @param message     of the error.
     * @param description of the error.
     */
    public RegisterErrorMessage(Map<String, String> message, String description) {
        logger.info("Registration error thrown with {} messages.", message.size());

        this.statusCode = HttpStatus.BAD_REQUEST.value();
        this.message = message;
        this.description = description;
    }

}
