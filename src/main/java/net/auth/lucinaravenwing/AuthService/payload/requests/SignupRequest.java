package net.auth.lucinaravenwing.AuthService.payload.requests;

import lombok.Getter;
import lombok.Setter;
import net.auth.lucinaravenwing.AuthService.validation.ValidEmail;
import net.auth.lucinaravenwing.AuthService.validation.ValidPassword;
import net.auth.lucinaravenwing.AuthService.validation.ValidUsername;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * Sign up Request template to define what is expected to be passed in from the user.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @since 3 May 2022
 */
@Getter
@Setter
public class SignupRequest {
    @ValidUsername
    @NotBlank
    @Size(min = 3, max = 20)
    private String username;

    @ValidEmail
    @NotBlank
    @Size(max = 50)
    @Email
    private String email;

    @ValidPassword
    @NotBlank
    private String password;

    /**
     * Default constructor for SignupRequest objects.
     */
    public SignupRequest() {
    }

    /**
     * All args constructor for SignupRequest objects.
     *
     * @param username of the user trying to register.
     * @param email    of the user trying to register.
     * @param password of the user trying to register (not yet encrypted).
     */
    public SignupRequest(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }
}