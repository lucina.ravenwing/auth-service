package net.auth.lucinaravenwing.AuthService.payload.responses;

import lombok.Getter;
import lombok.Setter;

/**
 * Response returned to the user when a request for a token refresh is sent in.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @since 3 May 2022
 */
@Getter
@Setter
public class RefreshTokenResponse {

    private String accessToken;
    private String refreshToken;

    /**
     * Default constructor for RefreshTokenResponse objects.
     */
    public RefreshTokenResponse() {
    }

    /**
     * All args constructor for RefreshTokenResponse objects.
     *
     * @param accessToken  of the token.
     * @param refreshToken of the token.
     */
    public RefreshTokenResponse(String accessToken, String refreshToken) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }
}
