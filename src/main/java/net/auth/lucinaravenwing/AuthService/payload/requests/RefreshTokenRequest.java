package net.auth.lucinaravenwing.AuthService.payload.requests;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * Token Refresh Request template to define what is expected to be passed in from the user.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @since 3 May 2022
 */
@Getter
@Setter
public class RefreshTokenRequest {
    @NotBlank
    private String refreshToken;

    /**
     * Default constructor for RefreshTokenRequest objects.
     */
    public RefreshTokenRequest() {
    }

    /**
     * All args constructor for RefreshTokenRequest objects.
     *
     * @param refreshToken token passed in by user to refresh their login.
     */
    public RefreshTokenRequest(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
