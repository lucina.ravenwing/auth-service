package net.auth.lucinaravenwing.AuthService.payload.requests;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * Password Reset Request template to define what is expected to be passed in from the user.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @since 3 May 2022
 */
@Getter
@Setter
public class PasswordResetRequest {

    @NotBlank
    private String password;

    /**
     * Default constructor for PasswordResetRequest objects.
     */
    public PasswordResetRequest() {
    }

    /**
     * All args constructor for PasswordResetRequest objects.
     *
     * @param password the updated password (unencrypted at this point).
     */
    public PasswordResetRequest(String password) {
        this.password = password;
    }
}
