package net.auth.lucinaravenwing.AuthService.payload.responses;

import lombok.Getter;
import lombok.Setter;

/**
 * Generic response returned to the user.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @since 3 May 2022
 */
@Getter
@Setter
public class GenericMessageResponse {

    private String message;

    /**
     * Default constructor for MessageResponse objects.
     */
    public GenericMessageResponse() {
    }

    /**
     * All args constructor for MessageResponse objects.
     *
     * @param message of the response.
     */
    public GenericMessageResponse(String message) {
        this.message = message;
    }

}
