package net.auth.lucinaravenwing.AuthService.payload.requests;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * Login Request template to define what is expected to be passed in from the user.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @since 3 May 2022
 */
@Getter
@Setter
public class LoginRequest {

    @NotBlank
    private String username;

    @NotBlank
    private String password;

    /**
     * Default constructor for LoginRequest objects.
     */
    public LoginRequest() {
    }

    /**
     * All args constructor for LoginRequest objects.
     *
     * @param username of the request.
     * @param password of the request.
     */
    public LoginRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
