package net.auth.lucinaravenwing.AuthService.payload.requests;

import lombok.Getter;
import lombok.Setter;

/**
 * Login Request template to define what is expected to be passed in from the user.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @since 3 May 2022
 */
@Getter
@Setter
public class LogoutRequest {

    private long userId;

    /**
     * Default constructor for LogoutRequest objects.
     */
    public LogoutRequest() {
    }

    /**
     * All args constructor for LogoutRequest objects.
     *
     * @param userId of the user to log out.
     */
    public LogoutRequest(Long userId) {
        this.userId = userId;
    }
}
