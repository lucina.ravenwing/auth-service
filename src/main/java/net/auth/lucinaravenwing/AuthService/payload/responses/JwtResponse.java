package net.auth.lucinaravenwing.AuthService.payload.responses;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Response returned to the user when a request for a JWT is sent in.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @since 3 May 2022
 */
@Getter
@Setter
public class JwtResponse {
    private String token;
    private final String type = "Bearer";
    private String refreshToken;
    private Long id;
    private String username;
    private String email;
    private List<String> roles;

    /**
     * Default constructor for JwtResponse objects.
     */
    public JwtResponse() {
    }

    /**
     * All args constructor for JwtResponse objects.
     *
     * @param accessToken  of the JWT.
     * @param refreshToken of the JWT.
     * @param id           of the user.
     * @param username     of the user.
     * @param email        of the user.
     * @param roles        of the user.
     */
    public JwtResponse(String accessToken, String refreshToken, Long id, String username, String email,
                       List<String> roles) {
        this.token = accessToken;
        this.refreshToken = refreshToken;
        this.id = id;
        this.username = username;
        this.email = email;
        this.roles = roles;
    }

}
