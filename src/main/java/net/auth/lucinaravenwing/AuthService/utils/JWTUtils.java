package net.auth.lucinaravenwing.AuthService.utils;

import io.jsonwebtoken.*;
import net.auth.lucinaravenwing.AuthService.core.UserDetailsImpl;
import net.auth.lucinaravenwing.AuthService.models.Role;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashSet;

/**
 * Utilities for JWTs. Generates, validates, and pulls info from JWTs.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @since 3 May 2022
 */
@Component
public class JWTUtils {

    private static final Logger logger = LoggerFactory.getLogger(JWTUtils.class);

    @Value(value = "${net.lucinaravenwing.jwtSecret}")
    private String jwtSecret;

    @Value(value = "${net.lucinaravenwing.jwtExpirationMs}")
    private int jwtExpirationMs;

    /**
     * Generates a JWT given user information.
     *
     * @param userPrincipal UserDetailsImpl that holds the information for user.
     * @return JWT in string format.
     */
    public String generateJwt(UserDetailsImpl userPrincipal) {
        logger.debug("Building JWT.");

        return generateTokenFromUsernameAndRoles(userPrincipal.getUsername(), new HashSet(userPrincipal.getAuthorities()));
    }

    /**
     * Generate the JWT given a username and user roles.
     *
     * @param username username of the user of the JWT.
     * @param roles    roles of the user of the JWT.
     * @return JWT in string format.
     */
    public String generateTokenFromUsernameAndRoles(String username, HashSet<Role> roles) {
        logger.debug("Generating token.");

        return Jwts.builder()
                .setSubject(username)
                .claim("roles", roles)
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + jwtExpirationMs)).
                signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    /**
     * Gets the username by parsing the JWT.
     *
     * @param token user's JWT.
     * @return username of the JWT owner.
     */
    public String getUserNameFromJwt(String token) {
        logger.debug("Getting username from JWT.");

        return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject();
    }

    /**
     * Validates the JWT. Catch errors and log.
     *
     * @param authToken token to be validated.
     * @return whether the token is validated.
     * @see MalformedJwtException
     * @see ExpiredJwtException
     * @see UnsupportedJwtException
     * @see IllegalArgumentException
     */
    public boolean validateJwt(String authToken) {
        logger.debug("Validating JWT.");

        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);

            logger.debug("JWT validated.");

            return true;
        } catch (MalformedJwtException e) {
            logger.info("Invalid JWT token: {}", e.getMessage());
        } catch (ExpiredJwtException e) {
            logger.info("JWT token is expired: {}", e.getMessage());
        } catch (UnsupportedJwtException e) {
            logger.info("JWT token is unsupported: {}", e.getMessage());
        } catch (IllegalArgumentException e) {
            logger.info("JWT claims string is empty: {}", e.getMessage());
        }

        return false;
    }
}
