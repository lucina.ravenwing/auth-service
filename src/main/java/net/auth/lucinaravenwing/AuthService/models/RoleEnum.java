package net.auth.lucinaravenwing.AuthService.models;

/**
 * User role enumeration.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @since 3 May 2022
 */
public enum RoleEnum {

    /**
     * User role.
     */
    ROLE_USER,
    /**
     * Moderator role.
     */
    ROLE_MODERATOR,
    /**
     * Admin role.
     */
    ROLE_ADMIN
}
