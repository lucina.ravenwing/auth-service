package net.auth.lucinaravenwing.AuthService.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;

/**
 * RefreshToken entity to retrieve RefreshToken information from the database.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @since 3 May 2022
 */
@Entity(name = "refresh_token")
@Getter
@Setter
public class RefreshToken {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @Column(name = "token", nullable = false, unique = true)
    private String token;

    @Column(name = "expiry_date", nullable = false)
    private Instant expiryDate;

    /**
     * Default constructor for RefreshToken objects.
     */
    public RefreshToken() {
    }

    /**
     * All args constructor for RefreshToken objects.
     *
     * @param id         of the RefreshToken.
     * @param user       owner of the RefreshToken.
     * @param token      actual token.
     * @param expiryDate of the RefreshToken.
     */
    public RefreshToken(long id, User user, String token, Instant expiryDate) {
        this.id = id;
        this.user = user;
        this.token = token;
        this.expiryDate = expiryDate;
    }
}
