package net.auth.lucinaravenwing.AuthService.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Role entity to retrieve role information from the database.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @since 3 May 2022
 */
@Entity
@Table(name = "roles")
@Getter
@Setter
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "name", length = 20)
    private RoleEnum name;

    /**
     * Default constructor for Role objects.
     */
    public Role() {
    }

    /**
     * All arg constructor for Role objects.
     *
     * @param id   of the role.
     * @param name of the role.
     */
    public Role(long id, RoleEnum name) {
        this.id = id;
        this.name = name;
    }
}
