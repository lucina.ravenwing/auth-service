package net.auth.lucinaravenwing.AuthService.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;

/**
 * PasswordResetToken entity to retrieve PasswordResetToken information from the database.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @since 3 May 2022
 */
@Entity(name = "password_reset_token")
// Lombok annotations
@Getter
@Setter
public class PasswordResetToken {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "token")
    private String token;

    @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "user_id")
    private User user;

    @Column(name = "expiry_date")
    private Date expiryDate;

    /**
     * Default constructor for PasswordResetToken objects.
     */
    public PasswordResetToken() {
        this.token = "";
        this.expiryDate = new Date(System.currentTimeMillis() - 1000);
    }

    /**
     * Partial constructor for PasswordResetToken objects.
     *
     * @param user  owner of the PasswordResetToken.
     * @param token token to verify if the caller has the rights to reset password.
     */
    public PasswordResetToken(User user, String token) {
        this.user = user;
        this.token = token;
        this.expiryDate = new Date(System.currentTimeMillis() + 3600000);
    }

    /**
     * All args constructor for PasswordResetToken objects.
     *
     * @param id         id of the token.
     * @param token      token used to verify if the caller has the rights to reset password.
     * @param user       owner of the PasswordResetToken.
     * @param expiryDate of the token.
     */
    public PasswordResetToken(long id, String token, User user, Date expiryDate) {
        this.id = id;
        this.token = token;
        this.user = user;
        this.expiryDate = expiryDate;
    }
}
