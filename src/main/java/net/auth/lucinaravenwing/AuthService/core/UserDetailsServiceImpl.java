package net.auth.lucinaravenwing.AuthService.core;

import net.auth.lucinaravenwing.AuthService.models.User;
import net.auth.lucinaravenwing.AuthService.repos.UserRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Implementation of the Spring's UserDetailsService Interface. Used to house and load user specific data.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @since 3 May 2022
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    // TRACE, DEBUG, INFO, WARN, ERROR, FATAL
    private static final Logger logger = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    private final UserRepo userRepo;

    /**
     * Autowired constructor for UserDetailsServiceImpl objects.
     *
     * @param userRepo repository to pull user data.
     */
    @Autowired
    public UserDetailsServiceImpl(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    /**
     * Loads users from userRepository by username.
     *
     * @param username username of the user to pull from database.
     * @return UserDetails object that contains the user's information.
     * @throws UsernameNotFoundException thrown when the username does not exist.
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        logger.debug("Retrieving user.");

        User user = userRepo.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));

        return UserDetailsImpl.build(user);
    }
}
