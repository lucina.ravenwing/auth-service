package net.auth.lucinaravenwing.AuthService.core;

import com.fasterxml.jackson.annotation.JsonIgnore;
import net.auth.lucinaravenwing.AuthService.models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Core user information implementation.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @since 3 May 2022
 */
public class UserDetailsImpl implements UserDetails {
    // TRACE, DEBUG, INFO, WARN, ERROR, FATAL
    private static final Logger logger = LoggerFactory.getLogger(UserDetailsImpl.class);

    private static final long serialVersionUID = 1L;

    /**
     * ID of the user in the database.
     */
    private final Long id;

    /**
     * Username of the user in the database.
     */
    private final String username;

    /**
     * Email of the user in the database.
     */
    private final String email;

    // JSONIgnore annotation used so the password will not be sent in any responses.
    /**
     * Password of the user in the database.
     */
    @JsonIgnore
    private final String password;

    /**
     * Roles of the user in the database.
     */
    private final Collection<? extends GrantedAuthority> authorities;

    /**
     * If the user is enabled (verified) in the database.
     */
    private final boolean enabled;

    /**
     * All args constructor for UserDetailsImpl objects.
     *
     * @param id          user id in database.
     * @param username    username of stored user.
     * @param email       email of stored user.
     * @param password    password of stored user (encrypted).
     * @param authorities authorities of stored user.
     * @param enabled     whether the stored user is enabled (verified).
     */
    public UserDetailsImpl(Long id, String username, String email, String password,
                           Collection<? extends GrantedAuthority> authorities, boolean enabled) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.authorities = authorities;
        this.enabled = enabled;
    }

    /**
     * Builds the UserDetails given a user.
     *
     * @param user user to build.
     * @return built UserDetailsImpl object.
     */
    public static UserDetailsImpl build(User user) {
        logger.debug("Building user details.");

        List<GrantedAuthority> authorities = user.getRoles().stream()
                .map(role -> new SimpleGrantedAuthority(role.getName().name()))
                .collect(Collectors.toList());

        return new UserDetailsImpl(
                user.getId(),
                user.getUsername(),
                user.getEmail(),
                user.getPassword(),
                authorities,
                user.isEnabled());
    }

    /**
     * Gets the authorities of a UserDetailsImpl object.
     *
     * @return a collection of authorities.
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    /**
     * Get the id of a UserDetailsImpl object.
     *
     * @return user id.
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets the email of a UserDetailsImpl object.
     *
     * @return user email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Gets the encrypted password of a UserDetailsImpl object.
     *
     * @return encrypted user password.
     */
    @Override
    public String getPassword() {
        return password;
    }

    /**
     * Gets the username of a UserDetailsImpl object.
     *
     * @return user username.
     */
    @Override
    public String getUsername() {
        return username;
    }

    /**
     * Gets whether the account is expired.
     *
     * @return user expired status.
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * Gets whether the account is locked.
     *
     * @return user locked status.
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * Gets whether the account is expired.
     *
     * @return user expired status.
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * Gets whether the account is enabled (verified).
     *
     * @return user enabled (verified) status.
     */
    @Override
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Compares two objects.
     *
     * @param o object to compare.
     * @return whether the objects are equal.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        UserDetailsImpl user = (UserDetailsImpl) o;
        return Objects.equals(id, user.id);
    }
}
