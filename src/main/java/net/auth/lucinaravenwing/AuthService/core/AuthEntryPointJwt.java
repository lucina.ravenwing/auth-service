package net.auth.lucinaravenwing.AuthService.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Class that sends an error if a user does not have access to a given endpoint. Will return an error that the user is
 * not verified or if they are not authorized.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @since 3 May 2022
 */
@Component
public class AuthEntryPointJwt implements AuthenticationEntryPoint {
    private static final Logger logger = LoggerFactory.getLogger(AuthEntryPointJwt.class);

    /**
     * Sends back an error when a user attempts to access a page they don't have access to or if the account is not
     * verified.
     *
     * @param request       user request to the server.
     * @param response      server response.
     * @param authException exception thrown that led to this method.
     * @throws IOException in the event of an IO Exception.
     */
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
                         AuthenticationException authException) throws IOException {
        logger.info("Unauthorized error: {} Trying to access {}", authException.getMessage(), request.getRequestURI());

        if (authException.getMessage().contains("disabled")) {
            response.setStatus(403);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write("{\"status\": 403, \"path\": \"\" + request.getRequestURI() + \"\", " +
                    "\"message\": \"Error: User is not verified.\"");
            response.getWriter().flush();
        } else {
            response.setStatus(401);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write("{\"status\": 401, \"path\": \"" + request.getRequestURI() + "\", " +
                    "\"message\": \"Error: Unauthorized.\"}");
            response.getWriter().flush();
        }
    }
}
