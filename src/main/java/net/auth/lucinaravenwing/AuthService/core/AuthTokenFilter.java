package net.auth.lucinaravenwing.AuthService.core;

import lombok.NonNull;
import net.auth.lucinaravenwing.AuthService.utils.JWTUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Class that parses JWT and authenticates the user given the roles.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @see JWTUtils
 * @since 3 May 2022
 */
public class AuthTokenFilter extends OncePerRequestFilter {

    private static final Logger logger = LoggerFactory.getLogger(AuthTokenFilter.class);

    @Autowired
    private JWTUtils jwtUtils;
    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    /**
     * Filters the request, checks the JWT, and either authenticates or blocks the user.
     *
     * @param request     user request to the server.
     * @param response    response from the server.
     * @param filterChain chain of the filters for authentication.
     * @throws ServletException in case of a Servlet Exception.
     * @throws IOException      in case of an IO Exception.
     */
    @Override
    protected void doFilterInternal(@NonNull HttpServletRequest request, @NonNull HttpServletResponse response,
                                    @NonNull FilterChain filterChain) throws ServletException, IOException {
        logger.debug("Filtering request.");

        try {
            String jwt = parseJwt(request);

            logger.debug("JWT parsed: {}", jwt);

            if (jwt != null && jwtUtils.validateJwt(jwt)) {
                String username = jwtUtils.getUserNameFromJwt(jwt);

                UserDetails userDetails = userDetailsService.loadUserByUsername(username);
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                        userDetails, null, userDetails.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        } catch (Exception e) {
            logger.error("Cannot set user authentication: {}", e.getMessage());
        }

        logger.debug("User authentication completed.");

        filterChain.doFilter(request, response);
    }

    /**
     * Parses the JWT for the token.
     *
     * @param request user request to the server.
     * @return the bearer token as a string.
     */
    private String parseJwt(HttpServletRequest request) {
        logger.debug("Parsing JWT.");

        String headerAuth = request.getHeader("Authorization");

        // Gets the token info removing "Bearer " from the beginning, otherwise it is invalid.
        if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) {
            return headerAuth.substring(7);
        }

        return null;
    }
}
