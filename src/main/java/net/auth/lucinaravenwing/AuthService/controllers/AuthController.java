package net.auth.lucinaravenwing.AuthService.controllers;

import net.auth.lucinaravenwing.AuthService.models.User;
import net.auth.lucinaravenwing.AuthService.payload.requests.LoginRequest;
import net.auth.lucinaravenwing.AuthService.payload.requests.LogoutRequest;
import net.auth.lucinaravenwing.AuthService.payload.requests.RefreshTokenRequest;
import net.auth.lucinaravenwing.AuthService.payload.requests.SignupRequest;
import net.auth.lucinaravenwing.AuthService.services.AuthService;
import net.auth.lucinaravenwing.AuthService.services.RefreshTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * Controller for authorization endpoints.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @see User
 * @see AuthService
 * @since 3 May 2022
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1")
public class AuthController {

    private final AuthService authService;
    private final RefreshTokenService refreshTokenService;

    /**
     * Autowired controller for AuthController objects.
     *
     * @param authService           authorization service that deals with Auth business logic.
     * @param refreshTokenService   service that handles refresh tokens.
     */
    @Autowired
    public AuthController(AuthService authService, RefreshTokenService refreshTokenService) {
        this.authService = authService;
        this.refreshTokenService = refreshTokenService;
    }

    /**
     * Endpoint to authenticate a user for the application suite.
     *
     * @param loginRequest   object with user info needed for login.
     * @param ignoredRequest user request to the server.
     * @return response to the user regarding success or failure.
     */
    @PostMapping("/public/auth/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest, HttpServletRequest ignoredRequest) {
        return this.authService.logUserIn(loginRequest);
    }

    /**
     * Endpoint to register a user for the application suite.
     *
     * @param signUpRequest object with user info needed for registering a user.
     * @param request       user request to the server.
     * @return response to the user regarding success or failure.
     */
    @PostMapping("/public/auth/register")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest, HttpServletRequest request) {
        String siteUrl = this.getSiteURL(request).replace(request.getServletPath(), "");

        return this.authService.registerUser(siteUrl, signUpRequest);
    }

    /**
     * Endpoint to refresh a JWT.
     *
     * @param tokenRequest   object with info needed to refresh a JWT.
     * @param ignoredRequest user request to the server.
     * @return response to the user regarding success or failure.
     */
    @PostMapping("/public/auth/refreshToken")
    public ResponseEntity<?> refreshToken(@Valid @RequestBody RefreshTokenRequest tokenRequest, HttpServletRequest ignoredRequest) {
        return this.authService.refreshToken(tokenRequest);
    }

    /**
     * Endpoint to log a user out of the application.
     *
     * @param logOutRequest  object with info needed to log a user out of application.
     * @param ignoredRequest user request to the server.
     * @return response to the user regarding success or failure.
     */
    @PostMapping("/private/auth/logout")
    @PreAuthorize("#logOutRequest.userId.equals(authentication.principal.id)")
    public ResponseEntity<?> logoutUser(@Valid @RequestBody LogoutRequest logOutRequest, HttpServletRequest ignoredRequest) {
        return this.refreshTokenService.deleteByUserId(logOutRequest.getUserId());
    }

    /**
     * Endpoint to validate a user before they can log in.
     *
     * @param userId           id of the user that needs to be verified.
     * @param verificationCode code that was generated previously to verify the user. Should match what's stored in the database.
     * @param ignoredRequest   user request to the server.
     * @return response to the user regarding success or failure.
     */
    @PostMapping("/public/auth/verify")
    public ResponseEntity<?> validateUser(@RequestParam("u") Long userId, @RequestParam("v") String verificationCode,
                                          HttpServletRequest ignoredRequest) {
        return authService.verifyUser(userId, verificationCode);
    }

    private String getSiteURL(HttpServletRequest request) {
        String siteUrl = request.getRequestURI();

        return "https://ygo.lucinaravenwing.net/" + siteUrl;
    }

}
