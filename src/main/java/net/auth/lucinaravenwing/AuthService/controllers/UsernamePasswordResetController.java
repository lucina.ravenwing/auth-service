package net.auth.lucinaravenwing.AuthService.controllers;

import net.auth.lucinaravenwing.AuthService.payload.requests.PasswordResetRequest;
import net.auth.lucinaravenwing.AuthService.services.UsernamePasswordResetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * Controller that allows a user to receive an email containing their username or a password reset link.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @since 3 May 2022
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1")
public class UsernamePasswordResetController {

    private final UsernamePasswordResetService usernamePasswordResetService;

    /**
     * Autowired constructor for UsernamePasswordResetController objects.
     *
     * @param usernamePasswordResetService username and password reset service that sends an email to users.
     */
    @Autowired
    public UsernamePasswordResetController(UsernamePasswordResetService usernamePasswordResetService) {
        this.usernamePasswordResetService = usernamePasswordResetService;
    }

    /**
     * Endpoint to allow a user to request an email with a link to reset their password.
     *
     * @param userEmail email address that the mail will be sent to.
     * @param request   user request to the server.
     * @return response to the user regarding success or failure.
     */
    @PostMapping("/public/auth/reset-password-request")
    public ResponseEntity<?> resetPasswordInitial(@RequestParam("email") String userEmail, HttpServletRequest request) {
        String siteUrl = this.getSiteURL(request).replace("/api/public/auth/reset-password-request", "");

        return usernamePasswordResetService.sendPasswordResetEmail(siteUrl, userEmail);
    }

    /**
     * Endpoint that will actually reset the user's password.
     *
     * @param resetToken           token generated to allow the user to verify they have authorization to update their password.
     * @param passwordResetRequest object that contains the info to properly reset user password.
     * @param ignoredRequest       user request to the server.
     * @return response to the user regarding success or failure.
     */
    @PostMapping("/public/auth/reset-password")
    public ResponseEntity<?> resetPassword(@RequestParam("t") String resetToken,
                                           @Valid @RequestBody PasswordResetRequest passwordResetRequest,
                                           HttpServletRequest ignoredRequest) {
        return this.usernamePasswordResetService.updatePassword(resetToken, passwordResetRequest.getPassword());
    }

    /**
     * Endpoint to allow user to request an email that contains their username.
     *
     * @param userEmail email address that the mail will be sent to.
     * @param request   user request to the server.
     * @return response to the user regarding success or failure.
     */
    @PostMapping("/public/auth/forgot-username")
    public ResponseEntity<?> forgotUsername(@RequestParam("email") String userEmail, HttpServletRequest request) {
        String siteUrl = this.getSiteURL(request).replace("/api/public/auth/forgot-username", "/login");

        return this.usernamePasswordResetService.sendUsernameInfo(siteUrl, userEmail);
    }

    private String getSiteURL(HttpServletRequest request) {
        String siteUrl = request.getRequestURI();

        return "https://ygo.lucinaravenwing.net" + siteUrl;

    }

}
