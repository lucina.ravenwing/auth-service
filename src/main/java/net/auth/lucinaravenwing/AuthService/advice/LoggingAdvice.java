package net.auth.lucinaravenwing.AuthService.advice;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/***
 * Advice for controller methods to log the API call.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @since 3 May 2022
 */

@Component
@Aspect
public class LoggingAdvice {

    private static final Logger logger = LoggerFactory.getLogger(LoggingAdvice.class);

    /***
     * Pointcut to allow the aspect to run a method at all controller classes.
     */
    @Pointcut("within(net.auth.lucinaravenwing.AuthService.controllers..*)")
    public void controllerMethods() {
    }

    /***
     * Pointcut to allow the aspect to run a method.
     */
    @Pointcut("execution(* *.*(..))")
    protected void allMethod() {
    }

    /***
     * Method to log what endpoint is being accessed in which method. Only runs for controller classes with a request arg.
     *
     * @param joinPoint of the method in the program.
     * @param request from the user that triggered the aspect.
     */
    @Before("controllerMethods() && allMethod() && args(.. , request)")
    public void logRequestURIAndMethodName(JoinPoint joinPoint, HttpServletRequest request) {
        logger.debug("Endpoint {} accessed with {} method.", request.getRequestURI(), joinPoint.getSignature().getName());
    }

    /***
     * Method to log what endpoint is being accessed in which method. Only runs for controller classes with a request arg.
     *
     * @param joinPoint of the method in the program.
     * @param ignoredRequest from the user that triggered the aspect.
     */
    @Before("controllerMethods() && allMethod() && args(.. , ignoredRequest)")
    public void logRequestURIAndMethodNameIgnored(JoinPoint joinPoint, HttpServletRequest ignoredRequest) {
        logger.debug("Endpoint {} accessed with {} method.", ignoredRequest.getRequestURI(), joinPoint.getSignature().getName());
    }
}

