package net.auth.lucinaravenwing.AuthService.advice;

import net.auth.lucinaravenwing.AuthService.messages.RegisterErrorMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Used to send a Bad Request status back when the user incorrectly registers. The response will also give the errors.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @see MethodArgumentNotValidException
 * @since 3 May 2022
 */
@RestControllerAdvice
public class RegisterExceptionHandlerAdvice {

    // TRACE, DEBUG, INFO, WARN, ERROR, FATAL
    private static final Logger logger = LoggerFactory.getLogger(RegisterExceptionHandlerAdvice.class);

    // Return just a bad request since there isn't really a specific one for register errors.

    /**
     * Returns a bad request error to the user.
     *
     * @param exception exception passed in to the method.
     * @param request   request that started the exception.
     * @return an error message that specifically handles the custom error logic added here.
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public RegisterErrorMessage handleValidationExceptions(MethodArgumentNotValidException exception, WebRequest request) {
        logger.info("RegisterErrorMessage method entered. Reason: {}", exception.getMessage());

        // Create a hashmap for the return of errors and an array to store the split message.
        Map<String, String> errors = new HashMap<>();
        String[] valueArray;

        // Get all the errors in the exception result.
        for (FieldError error : exception.getBindingResult().getFieldErrors()) {
            // Split the message by commas.
            if (error.getDefaultMessage() != null) {
                valueArray = error.getDefaultMessage().split(",");
            } else {
                continue;
            }

            // Store the values in an error hashmap to return to the frontend. Store them in applicable keys.
            for (String message : valueArray) {
                if (message.toLowerCase().contains("username")) {
                    if (message.contains("illegal")) {
                        errors.put("usernameSpecial", message.trim());
                    }
                    if (message.contains("length")) {
                        errors.put("usernameLength", message.trim());
                    }
                    if (message.contains("taken")) {
                        errors.put("usernameTaken", message.trim());
                    }
                }
                if (message.toLowerCase().contains("email")) {
                    errors.put("emailInUse", message.trim());
                }
                if (message.toLowerCase().contains("password")) {
                    if (message.contains("length")) {
                        errors.put("passwordLength", message.trim());
                    }
                    if (message.contains("uppercase")) {
                        errors.put("passwordUppercase", message.trim());
                    }
                    if (message.contains("lowercase")) {
                        errors.put("passwordLowercase", message.trim());
                    }
                    if (message.contains("digit")) {
                        errors.put("passwordNumber", message.trim());
                    }
                }
                if (message.toLowerCase().contains("blank")) {
                    errors.put("blankInput", "Username or password blank.");
                }
            }
        }

        // Return a register error with the created errors map and the description of the request.
        return new RegisterErrorMessage(
                errors,
                request.getDescription(false)
        );
    }

}
