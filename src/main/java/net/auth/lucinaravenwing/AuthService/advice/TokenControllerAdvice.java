package net.auth.lucinaravenwing.AuthService.advice;

import net.auth.lucinaravenwing.AuthService.exceptions.RefreshTokenException;
import net.auth.lucinaravenwing.AuthService.messages.GenericErrorMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

/**
 * Used to handle custom RefreshTokenException and send back a FORBIDDEN error.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @see RefreshTokenException
 * @since 3 May 2022
 */
@RestControllerAdvice
public class TokenControllerAdvice {

    // TRACE, DEBUG, INFO, WARN, ERROR, FATAL
    private static final Logger logger = LoggerFactory.getLogger(TokenControllerAdvice.class);

    /**
     * Exception to throw for refresh token issues. Sends back a forbidden status as the token is invalid.
     *
     * @param exception exception passed into the method.
     * @param request   request that started the exception.
     * @return custom error message that will be returned to the user.
     */
    @ExceptionHandler(value = RefreshTokenException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public GenericErrorMessage handleRefreshTokenException(RefreshTokenException exception, WebRequest request) {
        logger.warn("Invalid refresh token entered. Reason: {}", exception.getMessage());

        // Return an error message with a Forbidden code, the date, the exception message, and the description.
        return new GenericErrorMessage(
                HttpStatus.FORBIDDEN.value(),
                new Date(),
                exception.getMessage(),
                request.getDescription(false)
        );
    }
}
