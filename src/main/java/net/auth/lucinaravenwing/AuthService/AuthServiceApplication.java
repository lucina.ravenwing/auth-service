package net.auth.lucinaravenwing.AuthService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Starting point of the application.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @since 3 May 2022
 */
@SpringBootApplication
public class AuthServiceApplication {

    /**
     * Main function of the application.
     *
     * @param args arguments passed in from the command line.
     */
    public static void main(String[] args) {
        SpringApplication.run(AuthServiceApplication.class, args);
    }

}
