package net.auth.lucinaravenwing.AuthService.repos;


import net.auth.lucinaravenwing.AuthService.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * User repository to pull information from the database.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @see User
 * @since 3 May 2022
 */
@Repository
public interface UserRepo extends JpaRepository<User, Long> {
    /**
     * Find user based on user id.
     *
     * @param id to search.
     * @return optional User object.
     */
    Optional<User> findAllById(Long id);

    /**
     * Find user based on username.
     *
     * @param username to search.
     * @return optional User object.
     */
    Optional<User> findByUsername(String username);

    /**
     * Find user based on email.
     *
     * @param email to search
     * @return optional User object.
     */
    Optional<User> findByEmail(String email);

    /**
     * Determine if a user exists by given username.
     *
     * @param username to search.
     * @return whether the username exists.
     */
    Boolean existsByUsername(String username);

    /**
     * Determine if a user exists by a given email.
     *
     * @param email to search.
     * @return whether the email exists.
     */
    Boolean existsByEmail(String email);

}