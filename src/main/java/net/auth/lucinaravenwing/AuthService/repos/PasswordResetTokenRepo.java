package net.auth.lucinaravenwing.AuthService.repos;

import net.auth.lucinaravenwing.AuthService.models.PasswordResetToken;
import net.auth.lucinaravenwing.AuthService.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.Optional;

/**
 * Password Reset Token repository to pull information from the database.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @see PasswordResetToken
 * @since 3 May 2022
 */
@Repository
public interface PasswordResetTokenRepo extends JpaRepository<PasswordResetToken, Long> {

    /**
     * Retrieves a PasswordResetToken object based on the token.
     *
     * @param token string token used to verify a user has the right to update password.
     * @return optional PasswordResetToken object.
     */
    Optional<PasswordResetToken> findPasswordResetTokenByToken(String token);

    /**
     * Deletes all expired reset tokens for given user.
     *
     * @param date current date.
     * @param user user to delete.
     */
    void deletePasswordResetTokensByExpiryDateBeforeAndUser(Date date, User user);

}
