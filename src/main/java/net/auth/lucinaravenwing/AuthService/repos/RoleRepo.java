package net.auth.lucinaravenwing.AuthService.repos;

import net.auth.lucinaravenwing.AuthService.models.Role;
import net.auth.lucinaravenwing.AuthService.models.RoleEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Role repository to pull information from the database.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @see Role
 * @since 3 May 2022
 */
@Repository
public interface RoleRepo extends JpaRepository<Role, Long> {
    /**
     * Pull Role from database based on the role name.
     *
     * @param name role to pull from database.
     * @return optional role object.
     */
    Optional<Role> findByName(RoleEnum name);
}
