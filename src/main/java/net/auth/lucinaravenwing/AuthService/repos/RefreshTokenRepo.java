package net.auth.lucinaravenwing.AuthService.repos;

import net.auth.lucinaravenwing.AuthService.models.RefreshToken;
import net.auth.lucinaravenwing.AuthService.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Refresh Token repository to pull information from the database.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @see RefreshToken
 * @since 3 May 2022
 */
@Repository
public interface RefreshTokenRepo extends JpaRepository<RefreshToken, Long> {

    /**
     * Find a token object by the string token sent in.
     *
     * @param token string token from the user.
     * @return optional RefreshToken.
     */
    Optional<RefreshToken> findByToken(String token);

    /**
     * Deletes all tokens for a given user id.
     *
     * @param userId    of the tokens to delete.
     */
    void deleteAllByUserId(Long userId);
}
