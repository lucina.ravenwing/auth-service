package net.auth.lucinaravenwing.AuthService.services;


import net.auth.lucinaravenwing.AuthService.exceptions.RefreshTokenException;
import net.auth.lucinaravenwing.AuthService.models.RefreshToken;
import net.auth.lucinaravenwing.AuthService.payload.responses.GenericMessageResponse;
import net.auth.lucinaravenwing.AuthService.repos.RefreshTokenRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

/**
 * Service that houses the business logic for Refresh Token data.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @see RefreshToken
 * @since 3 May 2022
 */
@Service
public class RefreshTokenService {

    // TRACE, DEBUG, INFO, WARN, ERROR, FATAL
    private static final Logger logger = LoggerFactory.getLogger(RefreshTokenService.class);

    @Value("${net.lucinaravenwing.jwtRefreshExpirationMs}")
    private Long refreshTokenDurationMs;

    private final RefreshTokenRepo refreshTokenRepo;
    private final UserService userService;

    /**
     * Autowired constructor for RefreshTokenService objects.
     *
     * @param refreshTokenRepo  repository to save/retrieve refresh token information.
     * @param userService       service to access user information.
     */
    @Autowired
    public RefreshTokenService(RefreshTokenRepo refreshTokenRepo, UserService userService) {
        this.refreshTokenRepo = refreshTokenRepo;
        this.userService = userService;
    }

    /**
     * Finds the RefreshToken object by the String token.
     *
     * @param token to search.
     * @return optional RefreshToken object.
     */
    public Optional<RefreshToken> findByToken(String token) {
        logger.debug("Finding token.");

        return refreshTokenRepo.findByToken(token);
    }

    /**
     * Creates a refresh token based on the user id.
     *
     * @param userId user id to create the refresh token.
     * @return RefreshToken object.
     */
    public RefreshToken createRefreshToken(Long userId) {

        logger.debug("Creating refresh token.");

        RefreshToken refreshToken = new RefreshToken();

        refreshToken.setUser(userService.getUserById(userId));
        refreshToken.setExpiryDate(Instant.now().plusMillis(refreshTokenDurationMs));
        refreshToken.setToken(UUID.randomUUID().toString());

        logger.debug("Refresh token created.");

        refreshToken = refreshTokenRepo.save(refreshToken);
        return refreshToken;
    }

    /**
     * Verifies if a RefreshToken is expired.
     *
     * @param token to verify expiration.
     * @return RefreshToken that was passed in.
     */
    public RefreshToken verifyExpiration(RefreshToken token) {
        logger.debug("Verifying expiration date.");

        if (token.getExpiryDate().compareTo(Instant.now()) < 0) {
            logger.info("JWT expired.");

            refreshTokenRepo.delete(token);
            throw new RefreshTokenException(token.getToken(), "Refresh token was expired. Please log in again.");
        }

        logger.debug("Refresh token expiration verified.");
        return token;
    }

    /**
     * Deletes all RefreshTokens for a given user.
     *
     * @param userId to delete tokens.
     * @return response regarding success or failure.
     */
    @Transactional
    public ResponseEntity<?> deleteByUserId(Long userId) {

        logger.debug("Deleting token for userId: {}", userId);

        refreshTokenRepo.deleteAllByUserId(userId);

        logger.debug("Token deleted.");

        return ResponseEntity.ok(new GenericMessageResponse("Log out successful!"));
    }
}
