package net.auth.lucinaravenwing.AuthService.services;

import net.auth.lucinaravenwing.AuthService.core.UserDetailsImpl;
import net.auth.lucinaravenwing.AuthService.exceptions.RefreshTokenException;
import net.auth.lucinaravenwing.AuthService.models.RefreshToken;
import net.auth.lucinaravenwing.AuthService.models.Role;
import net.auth.lucinaravenwing.AuthService.models.RoleEnum;
import net.auth.lucinaravenwing.AuthService.models.User;
import net.auth.lucinaravenwing.AuthService.payload.requests.LoginRequest;
import net.auth.lucinaravenwing.AuthService.payload.requests.RefreshTokenRequest;
import net.auth.lucinaravenwing.AuthService.payload.requests.SignupRequest;
import net.auth.lucinaravenwing.AuthService.payload.responses.GenericMessageResponse;
import net.auth.lucinaravenwing.AuthService.payload.responses.JwtResponse;
import net.auth.lucinaravenwing.AuthService.payload.responses.RefreshTokenResponse;
import net.auth.lucinaravenwing.AuthService.repos.RoleRepo;
import net.auth.lucinaravenwing.AuthService.utils.JWTUtils;
import net.bytebuddy.utility.RandomString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service that houses the business logic for Auth data.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @see User
 * @since 3 May 2022
 */

@Service
public class AuthService {

    // TRACE, DEBUG, INFO, WARN, ERROR, FATAL
    private static final Logger logger = LoggerFactory.getLogger(AuthService.class);

    // Utilities
    private final AuthenticationManager authenticationManager;
    private final JWTUtils jwtUtils;
    private final PasswordEncoder encoder;

    // Repositories
    private final UserService userService;
    private final RoleRepo roleRepo;

    // Other services
    private final RefreshTokenService refreshTokenService;
    private final EmailService emailService;

    /**
     * Autowired constructor for AuthService object.
     *
     * @param authenticationManager core authentication object.
     * @param jwtUtils              utilities for JTWs.
     * @param encoder               password encoder.
     * @param roleRepo              repository for pulling user data.
     * @param refreshTokenService   service used to refresh token.
     * @param emailService          service used to retrieve/save email information.
     * @param userService           service for accessing user data.
     */
    @Autowired
    public AuthService(AuthenticationManager authenticationManager, JWTUtils jwtUtils, PasswordEncoder encoder,
                       RoleRepo roleRepo, RefreshTokenService refreshTokenService, EmailService emailService,
                       UserService userService) {
        this.authenticationManager = authenticationManager;
        this.jwtUtils = jwtUtils;
        this.userService = userService;
        this.encoder = encoder;
        this.roleRepo = roleRepo;
        this.refreshTokenService = refreshTokenService;
        this.emailService = emailService;
    }

    /**
     * Authenticates a user for logging in.
     *
     * @param loginRequest request containing information needed to log a user in.
     * @return response detailing success or failure.
     */
    public ResponseEntity<?> logUserIn(LoginRequest loginRequest) {
        logger.debug("Authenticating user with username {}.", loginRequest.getUsername());

        // This will cause an error in AuthTokenFilter's doFilter method if log in info is incorrect.
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        String jwt = jwtUtils.generateJwt(userDetails);

        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        RefreshToken refreshToken = refreshTokenService.createRefreshToken(userDetails.getId());

        logger.debug("User authenticated.");

        // Uses a JWT response as the front end only needs the JWT to send back in to verify a proper user.
        return ResponseEntity.ok(new JwtResponse(jwt,
                refreshToken.getToken(),
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                roles));
    }

    /**
     * Registers a user.
     *
     * @param siteUrl       URL used for sending the verification email.
     * @param signUpRequest request containing information needed to register a user.
     * @return response detailing success or failure.
     */
    @Transactional
    public ResponseEntity<?> registerUser(String siteUrl, SignupRequest signUpRequest) {
        logger.debug("Registering user {}.", signUpRequest.getUsername());

        // Create new user's account
        User user = new User(signUpRequest.getUsername(),
                signUpRequest.getEmail(),
                encoder.encode(signUpRequest.getPassword()));

        user.setEnabled(false);
        user.setVerificationCode(RandomString.make(16));

        Set<Role> roles = new HashSet<>();

        // If there's going to be an admin or moderator, this will be modified directly in the database.
        Role userRole = roleRepo.findByName(RoleEnum.ROLE_USER).orElseThrow(() -> new RuntimeException("Error: " +
                "Role not found."));
        roles.add(userRole);

        user.setRoles(roles);

        userService.saveUser(user);

        logger.debug("Sending email to user.");

        emailService.sendAuthEmail(siteUrl, user);

        return ResponseEntity.ok(new GenericMessageResponse("User registered successfully!"));
    }

    /**
     * Refreshes the token to keep user logged in.
     *
     * @param request to refresh token.
     * @return response detailing success or failure.
     */
    public ResponseEntity<?> refreshToken(RefreshTokenRequest request) {
        logger.debug("Refreshing Token.");

        String requestRefreshToken = request.getRefreshToken();

        return refreshTokenService.findByToken(requestRefreshToken)
                .map(refreshTokenService::verifyExpiration)
                .map(RefreshToken::getUser)
                .map(user -> {
                    String token = jwtUtils.generateTokenFromUsernameAndRoles(user.getUsername(), (HashSet<Role>) user.getRoles());
                    return ResponseEntity.ok(new RefreshTokenResponse(token, requestRefreshToken));
                })
                .orElseThrow(() -> new RefreshTokenException(requestRefreshToken, "Refresh token is not in the " +
                        "database!"));
    }

    /**
     * Verifies the user and sets the enabled flag to true.
     *
     * @param userId           of the user to verify.
     * @param verificationCode to compare to the database to verify the user.
     * @return response detailing success or failure.
     */
    public ResponseEntity<?> verifyUser(Long userId, String verificationCode) {
        logger.debug("Verifying user.");

        User user = userService.findAllById(userId);

        // TODO: Update this to return a custom Response class.
        if (user.getUsername().equals("DNE")) {
            logger.info("User with ID {} does not exist in the database.", userId);
            return new ResponseEntity<>("No user exists with given ID.", HttpStatus.BAD_REQUEST);
        } else if (!user.getVerificationCode().equals(verificationCode)) {
            logger.info("Verification code sent in does not match code in database. Given: UserID: {}, Code: {}. " +
                            "Expected: UserID: {}, Code: {}", userId, verificationCode, user.getId(),
                    user.getVerificationCode());
            return new ResponseEntity<>("Given verification code does not match stored value.", HttpStatus.BAD_REQUEST);
        }

        user.setVerificationCode("");
        user.setEnabled(true);
        userService.saveUser(user);

        logger.debug("User successfully verified.");

        return ResponseEntity.ok(new GenericMessageResponse("User successfully validated."));

    }

}
