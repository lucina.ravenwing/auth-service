package net.auth.lucinaravenwing.AuthService.services;

import net.auth.lucinaravenwing.AuthService.models.PasswordResetToken;
import net.auth.lucinaravenwing.AuthService.models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * Service that houses the business logic for Email data.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @since 3 May 2022
 */
@Service
public class EmailService {

    // TRACE, DEBUG, INFO, WARN, ERROR, FATAL
    private static final Logger logger = LoggerFactory.getLogger(EmailService.class);

    @Value("${net.lucinaravenwing.emailUsername}")
    private String sender;
    private String recipient;

    @Value("${net.lucinaravenwing.emailUsername}")
    private String senderUsername;
    @Value("${net.lucinaravenwing.emailPassword}")
    private String senderPassword;

    private final Properties PROPS = new Properties();

    /**
     * Default constructor for EmailService objects.
     */
    @Autowired
    public EmailService() {
        String HOST = "smtp.gmail.com";

        PROPS.put("mail.smtp.auth", "true");
        PROPS.put("mail.smtp.starttls.enable", "true");
        PROPS.put("mail.smtp.host", HOST);
        PROPS.put("mail.smtp.port", "587");
    }

    /**
     * Sends an authentication email to a user.
     *
     * @param siteUrl base URL of the site.
     * @param user    to send the authentication to.
     */
    public void sendAuthEmail(String siteUrl, User user) {
        recipient = user.getEmail();

        Session session = Session.getInstance(PROPS, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(senderUsername, senderPassword);
            }
        });

        try {
            logger.debug("Sending auth email.");

            String verifyURL = siteUrl + "/verify?u=" + user.getId() + "&v=" + user.getVerificationCode();

            Message message = new MimeMessage(session);

            message.setFrom(new InternetAddress(sender));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient));

            message.setSubject("Please verify your e-mail for Lucina Ravenwing's YGOCollection.");

            message.setContent("Welcome, " + user.getUsername() + ".<br><br>Click the link below to confirm your " +
                    "email address.<br><br><a href=" + verifyURL + ">Click here.</a>", "text/html");

            Transport.send(message);

            logger.debug("Auth email sent.");
        } catch (MessagingException e) {
            logger.warn(e.getMessage());
        }
    }

    /**
     * Sends a password reset email to a user.
     *
     * @param siteUrl    base URL of the site.
     * @param resetToken token stored in the database.
     */
    public void sendPasswordResetEmail(String siteUrl, PasswordResetToken resetToken) {
        recipient = resetToken.getUser().getEmail();

        Session session = Session.getInstance(PROPS, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(senderUsername, senderPassword);
            }
        });

        try {
            logger.debug("Sending password reset email.");

            String resetURL = siteUrl + "/reset-password?t=" + resetToken.getToken();

            Message message = new MimeMessage(session);

            message.setFrom(new InternetAddress(sender));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient));

            message.setSubject("Password reset for Lucina Ravenwing's YGOCollection.");

            message.setContent("A password reset request has been submitted for this email address. If you did not " +
                    "request this reset, you may ignore this email.<br>If you did request to reset your password, " +
                    "please press <a href=" + resetURL + ">this link.</a>", "text/html");

            Transport.send(message);

            logger.debug("Password reset email sent.");
        } catch (MessagingException e) {
            logger.warn(e.getMessage());
        }
    }

    /**
     * Sends an email with the user's username.
     *
     * @param siteUrl base URL of the site.
     * @param user    user to send the email to.
     */
    public void sendUsernameEmail(String siteUrl, User user) {
        recipient = user.getEmail();

        Session session = Session.getInstance(PROPS, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(senderUsername, senderPassword);
            }
        });

        try {
            logger.debug("Sending username info email.");

            Message message = new MimeMessage(session);

            message.setFrom(new InternetAddress(sender));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient));

            message.setSubject("Username for Lucina Ravenwing's YGOCollection.");

            message.setContent("A forgotten username request was submitted with this given email. If you did not " +
                    "request this notification, you may ignore this email. If you did request this email, your " +
                    "username for ygo.lucinaravenwing.net is <b>" + user.getUsername() + "</b>. <a href=" + siteUrl +
                    ">Click here</a> to log in.", "text/html");

            Transport.send(message);

            logger.debug("Username info email sent.");
        } catch (MessagingException e) {
            logger.warn(e.getMessage());
        }
    }

}
