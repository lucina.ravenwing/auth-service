package net.auth.lucinaravenwing.AuthService.services;

import net.auth.lucinaravenwing.AuthService.models.User;
import net.auth.lucinaravenwing.AuthService.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service used to access and store user info.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @see User
 * @since 14 July 2022
 */
@Service
public class UserService {

    private final UserRepo userRepo;

    /**
     * Autowired constructor for UserService class.
     *
     * @param userRepo  repository to access user data.
     */
    @Autowired
    public UserService(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    /**
     * Finds users in the database with a give id.
     *
     * @param id    to search.
     * @return      Users found in the database or a user with DNE as info.
     */
    public User findAllById(Long id) {
        return this.userRepo.findAllById(id).orElse(new User("DNE", "DNE", "DNE"));
    }

    /**
     * Retrieve the user from the repository by email.
     *
     * @param userEmail email to search.
     * @return either the user found from the database or a blank User.
     */
    public User getUserByEmail(String userEmail) {
        return userRepo.findByEmail(userEmail).orElse(new User());
    }

    /**
     * Find a user by a given id.
     *
     * @param id    of the user.
     * @return      the user with the given id if found, or a new user if not.
     */
    public User getUserById(Long id) {
        return userRepo.findById(id).orElse(new User());
    }

    /**
     * Save the user into the database.
     *
     * @param user  to save to the database.
     */
    public void saveUser(User user) {
        this.userRepo.save(user);
    }

}
