package net.auth.lucinaravenwing.AuthService.services;

import net.auth.lucinaravenwing.AuthService.models.PasswordResetToken;
import net.auth.lucinaravenwing.AuthService.models.User;
import net.auth.lucinaravenwing.AuthService.payload.responses.GenericMessageResponse;
import net.auth.lucinaravenwing.AuthService.repos.PasswordResetTokenRepo;
import net.bytebuddy.utility.RandomString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Service that houses the business logic for Username retrieval and Password reset request. data.
 *
 * @author Lucina Ravenwing
 * @version 1.0
 * @see PasswordResetToken
 * @since 3 May 2022
 */
@Service
public class UsernamePasswordResetService {

    // TRACE, DEBUG, INFO, WARN, ERROR, FATAL
    private static final Logger logger = LoggerFactory.getLogger(UsernamePasswordResetService.class);

    // Services
    private final UserService userService;
    private final EmailService emailService;

    // Repos
    private final PasswordResetTokenRepo passwordResetTokenRepo;

    // Other
    private final PasswordEncoder passwordEncoder;

    /**
     * Autowired constructor for UsernamePasswordResetService objects.
     *
     * @param userService            service to access user data.
     * @param emailService           service to email a user.
     * @param passwordResetTokenRepo repository that retrieves/saves password reset information.
     * @param passwordEncoder        to encode reset passwords.
     */
    @Autowired
    public UsernamePasswordResetService(UserService userService, EmailService emailService,
                                        PasswordResetTokenRepo passwordResetTokenRepo,
                                        PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.emailService = emailService;
        this.passwordResetTokenRepo = passwordResetTokenRepo;
        this.passwordEncoder = passwordEncoder;
    }

    /**
     * Sends a password reset email to a user containing a link to a page that will ask them to enter their new password.
     *
     * @param siteUrl   base URL of the site.
     * @param userEmail of the user asking to reset password.
     * @return response detailing success or failure.
     */
    public ResponseEntity<?> sendPasswordResetEmail(String siteUrl, String userEmail) {
        User user = userService.getUserByEmail(userEmail);
        String message = "";

        logger.debug("User requested password reset.");

        if (user.getUsername() == null) {
            logger.info("User requested username when email does not exist.");
            message += "User with given email does not exist.";
        } else if (!user.isEnabled()) {
            logger.info("User requested username when user is not verified.");
            message += "User is not verified.";
        }

        if (!message.equals("")) {
            logger.info("Password reset email failed to send.");

            Map<String, String> dataMessage = new HashMap<>();

            dataMessage.put("message", message);

            return ResponseEntity.badRequest().body(dataMessage);
        }

        logger.debug("Creating reset token");

        String token = RandomString.make(32);

        // This is a "not". If the database returns an Object, then the token will equal whatever the token in the
        // database is. Otherwise, it will be set to "". This is to avoid the unfortunate possibility of duplicates.
        boolean existsInDatabase =
                !passwordResetTokenRepo.findPasswordResetTokenByToken(token).orElse(new PasswordResetToken()).getToken()
                        .equals("");

        while (existsInDatabase) {
            logger.error("I just want to know if this ever actually happens, so error.");

            token = RandomString.make(32);

            existsInDatabase =
                    !passwordResetTokenRepo.findPasswordResetTokenByToken(token).orElse(new PasswordResetToken()).getToken()
                            .equals("");
        }

        PasswordResetToken resetToken = createPasswordResetTokenForUser(user, token);

        logger.debug("Password reset token saved successfully.");

        emailService.sendPasswordResetEmail(siteUrl, resetToken);

        logger.info("Password reset email sent.");

        return ResponseEntity.ok(new GenericMessageResponse("Email sent successfully."));
    }

    /**
     * Sends a user an email containing their username based on their email.
     *
     * @param siteUrl   base URL of the site.
     * @param userEmail of the user asking for username.
     * @return response detailing success or failure.
     */
    public ResponseEntity<?> sendUsernameInfo(String siteUrl, String userEmail) {
        User user = userService.getUserByEmail(userEmail);
        String message = "";

        logger.info("User requested username.");

        if (user.getUsername() == null) {
            message += "User with given email does not exist.";
        } else if (!user.isEnabled()) {
            message += "User is not verified.";
        }

        if (!message.equals("")) {
            Map<String, String> dataMessage = new HashMap<>();

            dataMessage.put("message", message);

            return ResponseEntity.badRequest().body(dataMessage);
        }

        this.emailService.sendUsernameEmail(siteUrl, user);

        logger.info("Username email sent.");

        return ResponseEntity.ok("Email with username information sent successfully.");
    }

    /**
     * Saves the updated password to the database.
     *
     * @param resetToken  token to verify a user has the right to update password.
     * @param newPassword new password entered by user.
     * @return response detailing success or failure.
     */
    public ResponseEntity<?> updatePassword(String resetToken, String newPassword) {
        String message = "";
        PasswordResetToken prt = passwordResetTokenRepo.findPasswordResetTokenByToken(resetToken).orElse(new PasswordResetToken());

        logger.debug("User resetting password.");

        if (prt.getExpiryDate().before(new Date(System.currentTimeMillis()))) {
            logger.info("Expired token.");

            message += "Token is expired.";
        }

        if (!message.equals("")) {
            Map<String, String> messageMap = new HashMap<>();

            logger.info("Password update failed.");

            messageMap.put("message", message);

            return ResponseEntity.badRequest().body(messageMap);
        }

        newPassword = passwordEncoder.encode(newPassword);

        prt.getUser().setPassword(newPassword);

        logger.debug("Saving user with new password.");

        userService.saveUser(prt.getUser());

        logger.debug("User saved.");

        passwordResetTokenRepo.delete(prt);
        passwordResetTokenRepo.deletePasswordResetTokensByExpiryDateBeforeAndUser(new java.sql.Date(System.currentTimeMillis()), prt.getUser());

        logger.info("Password updated and password reset token deleted.");

        return ResponseEntity.ok("Password successfully updated.");
    }

    private PasswordResetToken createPasswordResetTokenForUser(User user, String token) {
        PasswordResetToken passwordResetToken = new PasswordResetToken(user, token);

        logger.debug("Saving password reset token.");

        return passwordResetTokenRepo.save(passwordResetToken);
    }
}
