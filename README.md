# Auth Services - Java

[![Build Status](https://jenkins.lucinaravenwing.net/buildStatus/icon?job=auth-service-java&style=plastic)](https://jenkins.lucinaravenwing.net/job/auth-service-java/)

## Repository link
https://gitlab.com/lucina.ravenwing/auth-service

## URL
https://auth.lucinaravenwing.net

## Description
Authentication services for lucinaravenwing.org sites. Will authenticate the user and return a token that will allow the user to use a single account for all different applications under the suite. 

## Languages
Java

## Resources Used
Spring Boot\
Hibernate\
PostgreSQL\
Docker\
nginx\
Rest API\
Jenkins

==================
