pipeline {
    agent { label 'auth' }
    stages {
        stage('build') {
            steps {
                echo 'Starting build stage.'
                updateGitlabCommitStatus name: 'build', state: 'pending'
                sh 'cd /home/jenkins/workspace/auth-service-java'
                sh 'rm -rf ./src/main/resources'
                sh 'mkdir ./src/main/resources; cp /home/jenkins/application.properties ./src/main/resources'
                sh 'mvn clean'
                sh 'mvn package -DskipTests'
            }
        }
        stage('test') {
            steps {
                echo 'Starting test stage.'
                sh 'mvn test'
            }
        }
        stage('release') {
            steps {
                echo 'Starting release stage. Includes:\n\t- Archive the old jar on the server.\n\t- Copy the new jar to the deployed folder. \n\t- Build the docker image locally.\n\t- Test that the deploy worked (WIP).\n\t- Push the image to Docker Hub.'

                // Archive old jar and move new jar to correct folder
                sh 'mv /home/jenkins/deploy/Auth-Service.jar /home/jenkins/archived'
                sh 'cp ./target/Auth-Service.jar /home/jenkins/deploy'

                // Docker deploy steps
                sh 'docker stop auth-service || true'
                sh 'docker rm auth-service || true'

                // Push image to docker Hub
                script {
                    docker.withRegistry('https://registry.hub.docker.com', 'jenkins-dockerhub') {
                        app = docker.build("lucinaravenwing/auth-service")
                        app.push("${env.BUILD_NUMBER}")
                        app.push("latest")
                    }
                }
            }
        }
        stage('deploy') {
            steps {
                echo 'Starting deploy stage. Includes:\n\t- Running the docker container.'
                sh 'docker run -d -it --network=host --name=auth-service lucinaravenwing-authservice'
                updateGitlabCommitStatus name: 'build', state: 'success'
            }
        }
    }
    post {
        always {
            junit allowEmptyResults: true, testResults: 'target/surefire-reports/*.xml'

            publishCoverage adapters: [jacocoAdapter('**/jacoco.xml')]

            archiveArtifacts allowEmptyArchive: true,
                artifacts: 'target/*.jar',
                followSymlinks: false,
                onlyIfSuccessful: true

            cleanWs()
        }
    }
}
