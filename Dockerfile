FROM openjdk:11

LABEL maintainer="lucinaravenwing.net@gmail.com"

WORKDIR /
ADD ./target/Auth-Service.jar Auth-Service.jar

EXPOSE 8080

CMD java -jar /Auth-Service.jar
